import 'package:bloc/bloc.dart';

class EstadoVerificacion{}
class Creandose extends EstadoVerificacion{}
class SolicitandoRaza extends EstadoVerificacion{}
class EsperandoConfirmacionRaza extends EstadoVerificacion{}
class MostrandoConfirmacionRaza extends EstadoVerificacion{}
class MostrandoRazaNoConfirmado extends EstadoVerificacion{}

class EventoVerificacion{}
class Creado extends EventoVerificacion{}
class RazaRecibido extends EventoVerificacion{}
class RazaConfirmada extends EventoVerificacion{}
class RazaNoConfirmada extends EventoVerificacion{}

class BlocVerificacion extends Bloc<EventoVerificacion, EstadoVerificacion> {
  BlocVerificacion() : super(Creandose()) {
    on<Creado>((event, emit) {
      emit(SolicitandoRaza());
    });
  }
}