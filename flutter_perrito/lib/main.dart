import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'caracteristicas/bloc.dart';
void main() {
  runApp(const Aplicacion());
}


class AplicacionInyectada extends StatelessWidget {
  const AplicacionInyectada({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => BlocVerificacion(),
      child: const Aplicacion(),
    );
  }
}


class Aplicacion extends StatelessWidget {
  const Aplicacion({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        body: Builder(builder: (context){
          return const Text("Inicio de la aplicacion");
        }),
      ),
    );
  }
}
