import 'package:flutter_perrito/caracteristicas/bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:bloc_test/bloc_test.dart';

void main() {
  blocTest<BlocVerificacion, EstadoVerificacion>(
    'evento= Creado estado=SolicitandoNombre',
    build: () => BlocVerificacion(),
    act: (bloc) => bloc.add(Creado()),
    expect: () => [isA<SolicitandoRaza>()],
  );
}